��          �      L      �  F   �            #        A     T     s  	   |     �     �     �     �     �     �     �     �  &   �         &  d   D     �     �  6   �     �  %        <  
   E  #   P  	   t     ~     �     �     �     �     �  &   �           
                                                      	                                         Compact, easy-to-use and privacy-compliant stats plugin for WordPress. Documentation Donate Entries in top lists only for today No data available. Number of entries in top lists Pageview Pageviews Period of data saving Settings Statify Support Top referers Top targets days https://pluginkollektiv.org https://wordpress.org/plugins/statify/ pluginkollektiv PO-Revision-Date: 2020-01-29 22:03:14+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha
Language: fr
Project-Id-Version: Plugins - Statify - Stable (latest release)
 Une extension de statistique légère, facile d’utilisation et respectueuse de la sphère privée. Documentation Faire un don Entrées dans la top liste pour aujourd'hui uniquement Aucune donnée disponible. Nombre d’entrées dans la top liste Page vue Pages vues Période de sauvegarde des données Réglages Statify Support Meilleurs référents Meilleurs contenus jours https://pluginkollektiv.org https://wordpress.org/plugins/statify/ pluginkollektiv 