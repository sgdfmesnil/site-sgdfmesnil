��          �            x     y  X   �  .   �           ;  
   K  [   V  
   �     �     �     �  2   �     '     +     2  0  9  1   j  d   �  9        ;     V     j  }   v     �               %  2   D     w     {     �                                                   
                        	    A block to embed a PDF Viewer. A simple and 100% free Gutenberg Block to display PDF Viewers / Readers on your website. Click on the PDF icon to replace the PDF file. Gutenberg PDF Viewer Block Height (pixels) PDF Viewer The PDF Viewer’s size is 100% / 700px by default. You can set custom (pixels) size below. Upload PDF Width (pixels) audrasjb https://jeanbaptisteaudras.com https://www.whodunit.fr/gutenberg-pdf-viewer-block pdf reader viewer PO-Revision-Date: 2019-01-29 14:40:45+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.4.0-alpha
Language: fr
Project-Id-Version: Plugins - Gutenberg PDF Viewer Block - Stable (latest release)
 Un bloc permettant d’embarquer une liseuse PDF. Un bloc Gutenberg simple et 100% gratuit pour afficher des lecteurs/liseuses PDF sur votre site web. Cliquer sur l’icône PDF pour remplacer le fichier PDF. Gutenberg PDF Viewer Block Hauteur (en pixels) Liseuse PDF Par défaut, la taille de la liseuse PDF est de 100% par 700px. Vous pouvez personnaliser ces tailles (en pixels) ci-dessous. Téléverser un PDF Largeur (en pixels) audrasjb https://jeanbaptisteaudras.com https://www.whodunit.fr/gutenberg-pdf-viewer-block pdf lecteur liseuse 